About:
less than 3 is a single screen local multiplayer game where the players compete against each other by collecting items, denying resources to the other player, manipulating the outcome in order to gain the affection and heart of an NPC <3

Instructions:
less than 3 is a single screen local multiplayer game where the players compete
against each other to pursue their �love interest�

Directions:
Player 1 � W, A,S & D keys to move around the level
Player 2 � Uses the arrow keys
Both players can do wall jumps
P � Reset a level [ undocumented if the players get stuck ]

Objective:
Traverse through the levels by collecting coins, power ups [ crates ] that can help you
along your pursuit for �love� . After you collect one coin, you must go at the top to
your �love interest� and give her the loot also you have a set time limit and must have
the highest score when the timer shuts down, based on the score the winner is
announced at the end of the round.

Find the game here - http://globalgamejam.org/2013/less-3

All new bugs to be reported on the issues page - https://bitbucket.org/yadu/less-than-3/issues

Credits:
Kinshuk Sunil - @kinshuksunil - Music

Rashi Chandra - @RashiChandra - Art

Sanchit Gulati - @sanchitgulati - Game design, Development & Audio

Vasu Chaturvedi - @1starArchie - Game design, Art & Audio

Yadu Rajiv - @yadurajiv - Game design, Development, Art & Audio

lessThan3 by Gamesoup - http://gamesoup.in is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 2.5 India License.