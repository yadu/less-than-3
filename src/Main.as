package 
{
	import india.gamedev.stag.*;
	import org.flixel.*;

	
	[SWF(width = "800", height = "480", backgroundColor = "#000000")]
	[Frame(factoryClass = "gameLoader")]
	
	/**
	 * ...
	 * @author Yadu Rajiv
	 */
	public class Main extends FlxGame
	{
		function Main() {	
			super(800, 480, logoSplash, 1);
			
			FlxG.flashFramerate = 60;
			
		}
	}
	
}