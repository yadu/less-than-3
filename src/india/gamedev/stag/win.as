package india.gamedev.stag
{
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	import org.flixel.FlxState;
	import org.flixel.FlxText;
	import org.flixel.FlxButton;
	
	/**
	 * ...
	 * @author Yadu Rajiv
	 */
	public class win extends FlxState 
	{
		private var _btnRestart:FlxButton;
		private var _btnMainMenu:FlxButton;
		override public function create():void 
		{
			super.create();
			
			FlxG.music.stop();
			FlxG.play(Resources.snd_win, FlxG.volume);

			var bg:FlxSprite = null;
			
			if (Resources.winner == "Player One Wins!") {
				bg = new FlxSprite(0, 0, Resources.img_player1win);
			} else if (Resources.winner == "Player Two Wins!") {
				bg = new FlxSprite(0, 0, Resources.img_player2win);
			} else { // No one Wins!
				bg = new FlxSprite(0, 0, Resources.img_draw);
			}
			add(bg);
			
			Resources.winner = "";
			
			/**
			 * adding a button with an anon call back function
			 */
			_btnRestart = new FlxButton(FlxG.width/2 - 140, FlxG.height - 28, "Restart", function():void {
				FlxG.fade(0xff000000, 0.3, function():void { // FlxG.fade also takes in a function as the thrid param which is called when the fade completes
					FlxG.switchState(new game); // switching states are as simple as calling 'switchState()' with an instance of the new state.
				});
			});
			_btnMainMenu = new FlxButton(FlxG.width/2 + 60, FlxG.height - 28, "MainMenu", function():void {
				FlxG.fade(0xff000000, 0.3, function():void { // FlxG.fade also takes in a function as the thrid param which is called when the fade completes
					FlxG.switchState(new splash); // switching states are as simple as calling 'switchState()' with an instance of the new state.
				});
			});
			
			add(_btnRestart);
			add(_btnMainMenu);
		}
		
		override public function update():void 
		{
			super.update();
		}
		
	}
	
}