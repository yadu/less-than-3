package india.gamedev.stag
{
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author Yadu Rajiv
	 */
	public class Daughter extends FlxSprite
	{
		
		public var playerOneLove:Number;
		public var playerTwoLove:Number;
		
		public function Daughter(X:Number = 0, Y:Number = 0,SimpleGraphic:Class = null) {
			super(X, Y, SimpleGraphic);
			
			playerOneLove = 0;
			playerTwoLove = 0;
		}
		
	}
	
}