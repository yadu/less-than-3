package 
{
	import org.flixel.FlxPoint;
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author Yadu Rajiv
	 */
	public class Dad extends FlxSprite 
	{
		public var track:Array; //who all to track
		
		public var range:Number; //range within which items will be tracked
		
		public var topLeft:FlxPoint;
		public var bottomRight:FlxPoint
		
		public var walkDirection:String;
		
		public function Dad(X:Number = 0, Y:Number = 0, SimpleGraphic:Class = null) {
			super(X, Y, SimpleGraphic);
			
			walkDirection = "right";
		}
		
		override public function update():void 
		{
			
			// walk randomly from left to right
			
			
			if (this.x <= topLeft.x) {
				this.x = topLeft.x + 1;
			}
			
			if (this.x >= bottomRight.x) {
				this.y = bottomRight.x -1;
			}
			
			super.update();
		}
	}
	
}