package india.gamedev.stag
{
	import flash.events.Event;
	import org.flixel.*;
	
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.display.StageQuality;	
	import flash.display.StageDisplayState;
	
	/**
	 * ...
	 * @author Yadu Rajiv
	 */
	public class logoSplash extends FlxState
	{
		
		private var timeTotal:Number;
		
		override public function create():void 
		{
			super.create();
			
			// no menu
			FlxG.stage.showDefaultContextMenu = false;
			
			// scaling for full screen
			FlxG.stage.scaleMode = StageScaleMode.SHOW_ALL;
			FlxG.stage.align = StageAlign.TOP;
			FlxG.stage.quality = StageQuality.HIGH;
			
			FlxG.stage.addEventListener(Event.RESIZE, window_resized);
			
			FlxG.bgColor = 0xffffffff;
			
			FlxG.flash(0xffffffff, 3);
			
			var bg:FlxSprite = new FlxSprite(FlxG.width/2 - 90, FlxG.height/2 - 90, Resources.logo);
			add(bg);
			
			timeTotal = 0;
			
			// comment this line and build for the web
			toggle_fullscreen(StageDisplayState.FULL_SCREEN);

		}
		
		override public function update():void 
		{
			//code
			
			timeTotal += FlxG.elapsed;
			
			if (timeTotal >= 3) {
				FlxG.fade(0xff00000000, 1, function():void {
					FlxG.switchState(new splash);
				});
			}
			
			super.update();
		}
		
		override public function destroy():void 
		{
			// NULLs here
			
			super.destroy();
		}
		
		// from - http://www.funstormgames.com/blog/2011/10/flixel-fullscreen-mode-and-resizeable-windows/

		 /*
         * !!!!!
         *
         * The code in the next 2 functions will make your window go fullscreen in 4 easy steps
         *
         * !!!!!
        */
 
        // This is called when the user clicks the button.
        // By default, it will go to fullscreen if windowed, and windowed if fullscreen
        // Use the Force parameter to force it to go to a specific mode
        private function toggle_fullscreen(ForceDisplayState:String=null):void {
 
            // 1. Change the size of the Flash window to fullscreen/windowed
            //    This is easily done by checking stage.displayState and then setting it accordingly
            if (ForceDisplayState) {
                FlxG.stage.displayState = ForceDisplayState;
            } else {
                if (FlxG.stage.displayState == StageDisplayState.NORMAL) {
                    FlxG.stage.displayState = StageDisplayState.FULL_SCREEN;
                } else {
                    FlxG.stage.displayState = StageDisplayState.NORMAL;
                }
            }
 
            // The next function contains steps 2-4
            window_resized();
        }
 
        // This is called every time the window is resized
        // It's a separate function than toggle_fullscreen because we want to call it when the window
        // size changed even if the user didn't click the fullscreen button (eg by pressing escape to exit fullscreen mode)
        private function window_resized(e:Event = null):void {
 
            // 2. Change the size of the Flixel game window
            //    We already changed the size of the Flash window, so now we need to update Flixel.
            //    Just update the FlxG dimensions to match the new stage dimensions from step 1
            FlxG.width = FlxG.stage.stageWidth / FlxCamera.defaultZoom;
            FlxG.height = FlxG.stage.stageHeight / FlxCamera.defaultZoom;
 
            // 3. Change the size of the Flixel camera
            //    Lastly, update the Flixel camera to match the new dimensions from the previous step
            //    This is so that the camera can see all of the freshly resized dimensions
            FlxG.resetCameras(new FlxCamera(0, 0, FlxG.width, FlxG.height));
 
            // 4. Optionally, depening on your game, you may need to update couple more things:
            //    - Reposition / resize things such as the Hud to match the new screen dimensions
            //    - Camera bounds to allow the camera to travel everywhere within the resized dimensions
            //    - World bounds to allow everything to collide within the resized dimensions
            //    - For more information on camera/world bounds/hud positioning check out our other tutorial at
            //              http://www.funstormgames.com/blog/2011/10/flixel-setting-up-game-dimensions-swf-camera-level-hud-mouse/
            //    Anyways, we're just going to update the fullscreen button and dimensions text
            //textDimensions.width = FlxG.width;
            //textDimensions.text = "Current resolution: " + FlxG.width + "*" + FlxG.height;
 
        }
		
	}
	
}