package india.gamedev.stag
{
	import org.flixel.*;
	
	/**
	 * ...
	 * @author Yadu Rajiv
	 */
	public class splash extends FlxState
	{
		override public function create():void 
		{
			super.create();
			
			// mouse show
			//FlxG.mouse.show();
			FlxG.playMusic(Resources.mus_menu, FlxG.volume);
			
			FlxG.flash(0xff000000, 2);
			
			var bg:FlxSprite = new FlxSprite(0,0,Resources.img_menubg);
			add(bg);
			
		}
		
		override public function update():void 
		{
			//code
			
			if (FlxG.keys.justPressed("SPACE")) {
				FlxG.fade(0xff000000, 0.3, function():void { // FlxG.fade also takes in a function as the thrid param which is called when the fade completes
					FlxG.switchState(new instructions); // switching states are as simple as calling 'switchState()' with an instance of the new state.
				});
			}
			
			super.update();
		}
		
		override public function destroy():void 
		{
			// NULLs here
			
			super.destroy();
		}
		
	}
	
}