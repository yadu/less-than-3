package india.gamedev.stag
{
	import org.flixel.*;
	
	/**
	 * ...
	 * @author Yadu Rajiv
	 */
	public class player extends FlxSprite 
	{
		
		private var m_jumpHeight:Number;
		
		private var m_wallStickTimer:Boolean;
		private var m_wallStickTime:Number;
		
		private var m_lastWall:Number;
		
		private var m_wallJumping:Boolean;
		
		private var m_gravity:Number;
		
		private var m_speed:FlxPoint;
		
		private var m_wallFloorJump:Boolean;
		
		private var m_wallHit:FlxPoint;
		
		public var time:Number;
		
		public var airTime:Number;
		
		public var jumping:Boolean;
		
		public var walking:Boolean;
		
		public var wallJumping:Boolean;
		
		public var doJump:Boolean;
		
		public var useAlt:Boolean;
		
		public var hasGift:Boolean;
		
		public var powerTick:uint;
		
		public var inverseControl:Boolean;
		
		public var itemType:String;
		public var powerType:String;
		public var itemValue:Number;
		public var powerActivated:Boolean;
		
		public var deadStick:Boolean;
		
		public var freezeTime:Number;
		
		public function player(X:Number = 0, Y:Number = 0, Color:uint = 0xffff2637, SimpleGraphic:Class = null) {
			super(X, Y, SimpleGraphic);
			
			if(SimpleGraphic == null) {
				this.makeGraphic(32, 32,Color);
				//this.makeGraphic(10,10, flxExtra.randomA255RGB());
			} else {
				this.color = Color
			}
			
			m_jumpHeight = 500; //340
			//m_jumpHeight = 500;
			
			m_gravity = 1000;
			
			//32x32
			m_speed = new FlxPoint(160, 420); //300,800? or 600?
			//m_speed = new FlxPoint(180, 580); //300,800? or 600?
			//m_speed = new FlxPoint(300, 800); //300,800? or 600?
			
			this.acceleration.y = m_gravity;
			this.maxVelocity = m_speed;
			this.drag = new FlxPoint(this.maxVelocity.x * 4, 0);
			
			m_wallStickTimer = false;
			m_wallStickTime = 0;
			
			m_wallJumping = false;
			
			m_wallFloorJump = false;
			
			m_wallHit = new FlxPoint();
			
			time = 0;
			
			health = 100;
			
			airTime = 0;
			
			jumping = false;
			
			wallJumping = false;
			
			walking = false;
			
			doJump = false;

			useAlt = false;
			
			itemType = "";
			
			powerType = "";
			
			powerTick = 0;
			
			itemValue = 0;
			
			deadStick = true;
			
			inverseControl = false;
			
			powerActivated = false;
			
			freezeTime = 0;
			
		}
		
		override public function update():void {
			
			if (freezeTime >= 2) {
				deadStick = false;
				freezeTime = 0;
			}

			if(!deadStick) {
			
				if (jumping) {
					airTime += FlxG.elapsed;
				} else {
					airTime = 0;
				}
				
				if (!walking) {
					//FlxG.camera.angle = 0;
				}
				
				/*
				if(FlxG.camera.angle <= 6 && FlxG.camera.angle >= -6 && !this.isTouching(FlxObject.WALL)) {
					FlxG.camera.angle += (acceleration.x / 300 * FlxG.elapsed);
					if (FlxG.camera.angle > 6) {
						FlxG.camera.angle = 6;
					}
					
					if (FlxG.camera.angle < -6) {
						FlxG.camera.angle = -6;
					}
				}
				*/
				
				
				//if(!hackInProgress) {
					
					if(!m_wallJumping) {
						this.acceleration.x = 0;
						if (this.isTouching(FlxObject.FLOOR) && this.velocity.y == 0) {
							if(this.velocity.x ==0) {
								//this.play("idle");
								walking = false;
							}
						} else {
							//this.play("jump");
							jumping = true;
							walking = false;
						}
					}
					if (!inverseControl)
					{
						if (FlxG.keys.pressed((useAlt?"D":"RIGHT"))) {
							this.acceleration.x = +this.maxVelocity.x * 4;
							this.facing = RIGHT;
							if (this.velocity.y == 0 && this.isTouching(FlxObject.FLOOR)) {
								//this.play("walk");
								walking = true;
							}
						}
						
						if (FlxG.keys.pressed((useAlt?"A":"LEFT"))) {
							this.acceleration.x = -this.maxVelocity.x * 4;
							this.facing = LEFT;
							if (this.velocity.y == 0 && this.isTouching(FlxObject.FLOOR)) {
								//this.play("walk");
								walking = true;
							}
						}
					}
					else
					{
						if (FlxG.keys.pressed((useAlt?"D":"RIGHT"))) {
							this.acceleration.x = -this.maxVelocity.x * 4;
							this.facing = RIGHT;
							if (this.velocity.y == 0 && this.isTouching(FlxObject.FLOOR)) {
								//this.play("walk");
								walking = true;
							}
						}
						
						if (FlxG.keys.pressed((useAlt?"A":"LEFT"))) {
							this.acceleration.x = +this.maxVelocity.x * 4;
							this.facing = LEFT;
							if (this.velocity.y == 0 && this.isTouching(FlxObject.FLOOR)) {
								//this.play("walk");
								walking = true;
							}
						}						
					}
					
					if (FlxG.keys.justPressed((useAlt?"W":"UP")) || doJump) {
						
						doJump = false;
						this.acceleration.y = m_gravity;
						//this.play("jump");
						jumping = true;
						walking = false;
						
						if (this.isTouching(FlxObject.FLOOR)) {
							if (this.isTouching(FlxObject.WALL)) { // checking if we jumped from the floor and touching the wall
								m_wallFloorJump = true;
							}
							this.velocity.y -= m_jumpHeight;
							FlxG.play(Resources.snd_jump, FlxG.volume);
						} else if (!(this.isTouching(FlxObject.FLOOR)) && m_wallStickTimer && ((x > (m_wallHit.x - 8)) &&  (x < (m_wallHit.x + 8)) )) {
							m_wallFloorJump = false;
							this.velocity.y = 0;
							this.velocity.y -= m_jumpHeight;
							m_wallStickTimer = false;
							m_wallStickTime = 0
							
							m_wallJumping = true;

							if (m_lastWall == FlxObject.RIGHT) { // jump left if wall is on right
								this.acceleration.x = this.velocity.x -= this.maxVelocity.x*2;
							} else if (m_lastWall == FlxObject.LEFT) { // jump right if wall is on left
								this.acceleration.x = this.velocity.x += this.maxVelocity.x*2;
							}
							
							FlxG.play(Resources.snd_jump, FlxG.volume);
						}
					}
					
					if (this.justTouched(FlxObject.WALL)) { // just touched a wall
						walking = false;
						jumping = false;
						airTime = 0;
						this.velocity.y = this.velocity.y / 2.5;
						m_wallStickTimer = true;
						m_wallHit = new FlxPoint(x, y);
						if (m_wallJumping) {
							FlxG.play(Resources.snd_hit, FlxG.volume);
						}
					}
					
					if (this.justTouched(FlxObject.FLOOR)) { // just touched a floor
						if (airTime >= 1.2) {
							hurt(10);
							FlxG.flash(0x66ff0000, 0.5);
							FlxG.shake(0.008, 0.2);
						}
						jumping = false;
						airTime = 0;
						m_wallFloorJump = false;
						this.acceleration.y = m_gravity;
						FlxG.play(Resources.snd_hit, FlxG.volume);
					}
					
					if (!(this.isTouching(FlxObject.FLOOR))) { // not touching floor
						if (this.isTouching(FlxObject.WALL)) { // and touching a wall
							//this.play("slide");
							jumping = false;
							airTime = 0;
							m_wallStickTimer = true;
							if(this.isTouching(FlxObject.LEFT)) {
								m_lastWall = FlxObject.LEFT;
							}
							if(this.isTouching(FlxObject.RIGHT)) {
								m_lastWall = FlxObject.RIGHT;
							}
						}
					} else { // touching floor
						m_wallJumping = false;
					}
					
					if (this.isTouching(FlxObject.WALL)) { // touching wall
						m_wallJumping = false;
						m_wallHit = new FlxPoint(x, y);
						jumping = false;
						walking = false;
						airTime = 0;
						if (this.isTouching(FlxObject.WALL) && !(this.isTouching(FlxObject.FLOOR))) { // and not touching ground
							if (m_wallFloorJump) { // if we jumped from the floor and touching the wall then don't let gravity change
								this.acceleration.y = m_gravity;
							} else {
								this.acceleration.y = m_gravity / 5;
							}
						}
					} else { // not touching wall 
						this.acceleration.y = m_gravity;
					}
					
					// wall stick timer
					if (m_wallStickTimer) {
						m_wallStickTime += FlxG.elapsed;
					}
					
					if (m_wallStickTime > 1.0) {
						m_wallStickTime = 0;
						m_wallStickTimer = false;
						this.acceleration.y = m_gravity;
					}
					
					if(facing == FlxObject.RIGHT) {
						if (jumping &&  this.velocity.y > 0) {
							this.angle = this.velocity.y / 60;
							//FlxG.camera.angle = this.angle;
						} else if (jumping && this.velocity.y < 0) {
							this.angle = this.velocity.y / 60;
							//FlxG.camera.angle = this.angle;
						} else {
							this.angle = 0;
						}
					} else {
						if (jumping &&  this.velocity.y > 0) {
							this.angle = -1 * this.velocity.y / 60;
							//FlxG.camera.angle = this.angle;
						} else if (jumping && this.velocity.y < 0) {
							this.angle = -1 * this.velocity.y / 60;
							//FlxG.camera.angle = this.angle;
						} else {
							this.angle = 0;
						}
					}
					
					wallJumping = m_wallStickTimer;
				
				//}
			} else {// deadStick
				
				this.velocity.x = 0;
				this.acceleration.x = 0;
				this.acceleration.y = m_gravity;
				freezeTime += FlxG.elapsed;
				
			}
			
			super.update();

		}
		
		override public function destroy():void 
		{
			m_jumpHeight = null;
			
			m_wallStickTimer = null;
			m_wallStickTime = null;
		
			m_lastWall = null;
		
			m_wallJumping = null;
		
			m_gravity = null;
		
			m_speed = null;
		
			m_wallFloorJump = null;
		
			m_wallHit = null;
			
			super.destroy();
		}
	}
	
}