package india.gamedev.stag
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import org.flixel.FlxBasic;
	import org.flixel.FlxGroup;
	import org.flixel.FlxObject;
	import org.flixel.FlxSprite;
	import org.flixel.FlxTileblock;
	import org.flixel.FlxU;
	
	/**
	 * ...
	 * @author Yadu Rajiv
	 */
	public class flxExtra
	{
		
		static public function randomARGB():uint {
			var a:uint = Math.floor(Math.random() * 254);
			var r:uint = Math.floor(Math.random() * 254);
			var g:uint = Math.floor(Math.random() * 254);
			var b:uint = Math.floor(Math.random() * 254);
			
			var argb:uint = a << 24 | r << 16 | g << 8 | b;
			
			return argb;
		}
		
		static public function randomA255RGB():uint {
			var a:uint = 255;
			var r:uint = Math.floor(Math.random() * 254);
			var g:uint = Math.floor(Math.random() * 254);
			var b:uint = Math.floor(Math.random() * 254);
			
			var argb:uint = a << 24 | r << 16 | g << 8 | b;
			
			return argb;
		}
		
		static public function randomRGB():uint {
			var r:uint = Math.floor(Math.random() * 254);
			var g:uint = Math.floor(Math.random() * 254);
			var b:uint = Math.floor(Math.random() * 254);
			
			var rgb:uint = r << 16 | g << 8 | b;
			
			return rgb;
		}
		
		static public function pngToColorCSV(PNGFile:Class,Scale:uint=1):String
		{
			//Import and scale image if necessary
			var layout:Bitmap;
			if(Scale <= 1)
				layout = new PNGFile;
			else
			{
				var tmp:Bitmap = new PNGFile;
				layout = new Bitmap(new BitmapData(tmp.width*Scale,tmp.height*Scale));
				var mtx:Matrix = new Matrix();
				mtx.scale(Scale,Scale);
				layout.bitmapData.draw(tmp,mtx);
			}
			var bd:BitmapData = layout.bitmapData;
			
			//Walk image and export pixel values
			var r:uint;
			var c:uint;
			var p:uint;
			var csv:String;
			var w:uint = layout.width;
			var h:uint = layout.height;
			for(r = 0; r < h; r++)
			{
				for(c = 0; c < w; c++)
				{
					//Decide if this pixel/tile is solid (1) or not (0)
					p = bd.getPixel(c,r);
					
					//Write the result to the string
					if(c == 0)
					{
						if(r == 0)
							csv += p;
						else
							csv += "\n"+p;
					}
					else
						csv += ", "+p;
				}
			}
			return csv;
		}
		
static public function BitmapToColorCSV(bd:BitmapData):String
		{		
			//Walk image and export pixel values
			var r:uint;
			var c:uint;
			var p:uint;
			var csv:String;
			var w:uint = bd.width;
			var h:uint = bd.height;
			for(r = 0; r < h; r++)
			{
				for(c = 0; c < w; c++)
				{
					//Decide if this pixel/tile is solid (1) or not (0)
					p = bd.getPixel(c,r);
					
					//Write the result to the string
					if(c == 0)
					{
						if(r == 0)
							csv += p;
						else
							csv += "\n"+p;
					}
					else
						csv += ", "+p;
				}
			}
			return csv;
		}
	
		static public function trim( s:String ):String {
			if(s != null)
				return s.replace( /^([\s|\t|\n]+)?(.*)([\s|\t|\n]+)?$/gm, "$2" );
			else
				return "";
		}
		
		static public function fix(i:Number):* {
			return (i < 10)?("0" + i.toString()):(i);
		}
		
		static public function fixms(i:Number):* {
			i = i * 1000;
			i =  FlxU.floor(i);
			if (i < 10) {
				return "00" + i.toString()
			} else if (i < 100) {
				return "0" + i.toString()
			}
			
			return i;
		}
	
		static public function setupObjects(objMap:String,objGroup:FlxGroup,objCreateCallback:Function,tileMapHeight:uint,tileMapWidth:uint,alignCenter:Boolean=true):void {
			
			var cols:Array;
			var rows:Array = objMap.split("\n");
			
			var heightInTiles:uint = rows.length;
			var widthInTiles:uint = 0;
			
			for(var r:uint = 0; r < heightInTiles; r++)
			{
				cols = rows[r].split(",");
				if(cols.length <= 1)
				{
					heightInTiles--;
					continue;
				}
				if(widthInTiles == 0)
					widthInTiles = cols.length;
				for (var c:uint = 0; c < widthInTiles; c++) {
					if (cols[c] != 0xffffff) {
					
						var tmpx:Number = c * tileMapWidth;
						var tmpy:Number = r * tileMapHeight;
						
						var tmp:* = objCreateCallback(cols[c], tmpx, tmpy);
						
						if(tmp != null) {
							if(alignCenter) {
								tmp.x = tmpx + ((tileMapWidth / 2) - (tmp.width / 2));
								tmp.y = tmpy + ((tileMapHeight / 2) - (tmp.height / 2));
								} else {
								tmp.x = tmpx;
								tmp.y = tmpy;
							}
							
							objGroup.add(tmp);
						}
					}
				}
			}
		}
	}
}