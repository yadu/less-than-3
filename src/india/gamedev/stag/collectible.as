package india.gamedev.stag 
{
	import flash.automation.StageCapture;
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author Yadu Rajiv
	 */
	public class collectible extends FlxSprite
	{
		
		// can be normal, special, key
		public var itemType:String;
		
		public var itemValue:Number;
		
		public function collectible(X:Number = 0, Y:Number = 0, Type:String = "gift",Value:Number = 5, SimpleGraphic:Class = null) {
			super(X, Y, SimpleGraphic);
			
			if (SimpleGraphic == null) {
				makeGraphic(16, 16, 0xff00ff00);//32x32
			}
			
			itemType = Type;
			itemValue = Value;
		}
		
		override public function destroy():void 
		{
			itemType = null;
			itemValue = null;
			super.destroy();
		}
	}
	
}