package india.gamedev.stag 
{
	import org.flixel.FlxSprite;
	/**
	 * ...
	 * @author Sanchit Gulati
	 */
	public class fire extends FlxSprite 
	{
		
		public function fire(X:Number = 0, Y:Number = 0, Color:uint = 0xffff2637, SimpleGraphic:Class = null) {
			super(X, Y, SimpleGraphic);
			this.makeGraphic(10, 10, Color);
			//this.color = Color;
		}
		
		override public function update():void 
		{
			super.update();
		}
		
		override public function destroy():void 
		{
			super.destroy();
		}
		
	}

}