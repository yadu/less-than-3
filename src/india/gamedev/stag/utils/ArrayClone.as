package india.gamedev.stag.utils 
{
	/**
	 * ...
	 * @author Sanchit Gulati
	 */
	
	import flash.utils.ByteArray; 
		
	public class ArrayClone 
	{
		static public function clone(source:Object):* 
		{ 
			var myBA:ByteArray = new ByteArray(); 
			myBA.writeObject(source); 
			myBA.position = 0; 
			return(myBA.readObject()); 
		}
		
	}

}