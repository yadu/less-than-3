package india.gamedev.stag.utils 
{
	import org.flixel.FlxU;
	/**
	 * ...
	 * @author Sanchit Gulati
	 * 0: Blank
	 * 1: Static Platform
	 * 2: Moving Horizontal Moving Platform
	 * 3: Moving Vertical Moving Platform
	 * 4: Player Spawn points for A
	 * 5: Player Spawn points for B
	 * 6: Gifts Spawn Points
	 * 7: Weapons Spawn Points
	 * 8: Alcoholic Fire
	 */
	public class FlxForestGenerator 
	{
		
		private var _numTilesCols:uint = 80;
		private var _numTilesRows:uint = 48;
		private var _initFireRatio:Number = 0.4;
		private var _initPlatformRatio:Number = 0.2;
		private var _initPlayerRatio:Number = 0.2;
		private var _initCollectableRatio:Number = 0.2;
		private var _initPlayerRatioDefault:Number = 0.2;
		private var _tetrisShapes:Array = new Array();
		private var _playerAPlaced:Boolean = false;
		private var _playerBPlaced:Boolean = false;
		private var _GPlaced:Boolean = false;
		private var _FCPlaced:Boolean = false;
		
		private var _seed:uint = 213435434;
		/**
		 * 
		 * @param	nCols	Number of columns in the Forest tilemap
		 * @param	nRows	Number of rows in the Forest tilemap
		 */
		public function FlxForestGenerator( nCols:uint = 80, nRows:uint = 48) 
		{
			_numTilesCols = nCols;
			_numTilesRows = nRows;
			//add Tetris Shapes;
			_tetrisShapes[0] = new Array();
			_tetrisShapes[1] = new Array();
			_tetrisShapes[2] = new Array();
			_tetrisShapes[3] = new Array();
			_tetrisShapes[4] = new Array();
			_tetrisShapes[5] = new Array();
			_tetrisShapes[0][0] = [1, 1, 1, 1, 1, 1];
			_tetrisShapes[0][1] = [0, 1, 1, 1, 1, 0];
			_tetrisShapes[0][2] = [1, 1, 1, 0, 0, 0];
			_tetrisShapes[1][0] = [1, 1, 0, 0, 1, 1];
			_tetrisShapes[1][1] = [1, 1, 1, 1, 1, 1];
			_tetrisShapes[2][0] = [1, 1, 1, 1, 1, 1];
			_tetrisShapes[2][1] = [1, 1, 1, 1, 1, 1];
			_tetrisShapes[2][2] = [1, 1, 0, 0, 1, 1];
			_tetrisShapes[3][0] = [1, 1, 1, 1, 1, 1];
			_tetrisShapes[3][1] = [1, 1, 1, 1, 0, 1];
			_tetrisShapes[3][2] = [1, 1, 0, 0, 0, 1];
			_tetrisShapes[4][0] = [1, 1, 1, 1, 1, 1];
			_tetrisShapes[4][1] = [0, 0, 1, 1, 1, 1];
			_tetrisShapes[4][2] = [0, 0, 0, 0, 1, 1];
			_tetrisShapes[5][0] = [1, 1, 1, 1, 1, 1];
			_tetrisShapes[5][1] = [1, 0, 1, 0, 0, 1];
			_tetrisShapes[5][2] = [1, 0, 1, 0, 0, 1];
			
		}
		
		/**
		 * @param 	mat		A matrix of data
		 * 
		 * @return 	A string that is usuable for FlxTileMap.loadMap(...)
		 */
		static public function convertMatrixToStr( mat:Array ):String
		{
			var mapString:String = "";
			
			for ( var y:uint = 0; y < mat.length; ++y )
			{
				for ( var x:uint = 0; x < mat[y].length; ++x )
				{
					mapString += mat[y][x].toString() + ",";
				}
				
				mapString += "\n";
			}
			
			return mapString;
		}
		
		static public function convertMatrixToStrBinaryOnly( matValue:Array ):String
		{
			var mapString:String = "";
			var mat:Array = matValue;
			for ( var y:uint = 0; y < mat.length; ++y )
			{
				for ( var x:uint = 0; x < mat[y].length; ++x )
				{
					if (mat[y][x] != 1)
						mat[y][x] = 0;
					mapString += mat[y][x].toString() + ",";
				}
				
				mapString += "\n";
			}
			
			return mapString;
		}
		
		/**
		 * 
		 * @param	rows 	Number of rows for the matrix
		 * @param	cols	Number of cols for the matrix
		 * 
		 * @return Spits out a matrix that is cols x rows, zero initiated
		 */
		private function genInitMatrix( rows:uint, cols:uint ):Array
		{
			// Build array of 1s
			var mat:Array = new Array();
			for ( var y:uint = 0; y < rows; ++y )
			{
				mat.push( new Array );
				for ( var x:uint = 0; x < cols; ++x ) mat[y].push(0);
			}
			
			return mat;
		}
		
		/**
		 * 
		 * @return Returns a matrix of a cave!
		 */
		public function generateForestLevel():Array
		{
			// Initialize random array
			var mat:Array = genInitMatrix( _numTilesRows, _numTilesCols );
			for ( var y:uint = 0; y < _numTilesRows; ++y )
			{
				//Count platforms in one one
				var countBlocks:uint = 0;
				/**
				 * Whether to add platform on this level
				 */
				var _platformRow:Boolean = false;
				if ( y == 15 || y == 25 || y == 32 || y == 39)
					_platformRow = true;
				
				//var _movingLift:Boolean = false;
				//if ( y == 20 || y == 30 || y == 40 || y == 50 || y == 60)
					//_movingLift = true;
					
				//only traverse the first half and duplicate everything on other side
				for ( var x:uint = 0; x < _numTilesCols/2; ++x ) 
				{
					/**
					 * Set platforms (static ones)
					 */
					if (_platformRow)
					{
						if (Math.random() < _initPlatformRatio)
						{
							var shapeNo:uint = FlxU.floor(Math.random() * 5); //hardcoded
							var flag:Boolean = false;
							
							for (var u:uint = x; u < _tetrisShapes[shapeNo][0].length ; ++u)
							{
								if (mat[y][x+u] == 1)
									flag = true;
							}
							//for checking5 steps forward and 5 steps backwards
							for (var r:uint = 1; r <= 5; r++ ) //hardcoded
							{
								if (mat[y][x + _tetrisShapes[shapeNo][0].length + r] == 1 || mat[y][x - r] == 1)
								{
									flag = true;
									break;
								}
							}
							if (!flag && countBlocks <= 5) //hardcoded
							{
								countBlocks++;
								for (var j:uint = 0; j < _tetrisShapes[shapeNo].length; j++)
								{
									for (var uu:uint = 0; uu < _tetrisShapes[shapeNo][j].length ; ++uu)
									{
										mat[y + j][x + uu] = _tetrisShapes[shapeNo][j][uu];
										
										//Also add a Mirror Image
										mat[y+j][_numTilesCols-x-uu-1] = _tetrisShapes[shapeNo][j][uu];
									}
								}
							}
						}
						
					}
				}
			}
			
			//Full Loop
			var lastFire:uint = 0;
			for ( var yy:uint = 0; yy < _numTilesRows; ++yy )
			{
				
				/**
				 * Whether to add spawn points
				 */
				var _SpawnRow:Boolean = false;
				if ( yy == 37 || yy == 34) //hardcoded
					_SpawnRow = true;
				 /**
				  * Whether to add Collectables
				  */
				 var _CollectablesRow:Boolean = false;
				 if ( yy == 13 || yy == 23 || yy == 30 || yy == 37)
					_CollectablesRow = true;
					
				for ( var xx:uint = 0; xx < _numTilesCols; ++xx )
				{
					/**
					 * all walls on four corners
					 */
					if (xx == 0 || yy == 0 || xx == _numTilesCols-1 || yy == _numTilesRows-1 )
					{
						/**overlord cancelled
						 * 
						 */
						 /*if ( yy == 0 && (xx >= 30&& xx <= 49))
							{
								mat[yy][xx] = 0;
								continue;
							}*/
						/**
						 * Check for bottom row,  add fire at random places (probability 8/10)
						 */
						if (yy == _numTilesRows-1 && xx != 0 && xx <= _numTilesCols-5 && xx > lastFire+12) //hardcoded value
						{
							mat[yy][xx] = ( Math.random() < _initFireRatio ? 8:1 ); //8 for fire
							if (mat[yy][xx] == 8)
							{
								mat[yy][xx + 1] = 8;
								mat[yy][xx + 2] = 8;
								mat[yy][xx + 3] = 8;
								mat[yy][xx + 4] = 8;
								mat[yy][xx + 5] = 8;
								mat[yy][xx + 6] = 8;
								lastFire = xx + 6;
								mat[yy][xx + 7] = (Math.random() < 0.5 ? 8:1 ); //half probability
								if (mat[yy][xx + 7] == 8 )
								{
									lastFire = xx + 7;
									if (Math.random() < 0.3)
									{
										mat[yy][xx + 8] = 8;
										lastFire = xx + 8;
									}
								}
							}
						}
						else if(yy == _numTilesRows-1 && (xx > lastFire || xx == 0 || xx == _numTilesCols-1))
							mat[yy][xx] = 1;
						else if(yy != _numTilesRows-1)
							mat[yy][xx] = 1;
					}
					/**
					 * Platform for the lady
					 */
					if ( (xx >= 35 && xx <= 45) && (yy == 7 || yy == 8) )
					{
						mat[yy][xx] = 1;
					}
					
					/**
					 * Set Player Spawns
					 */
					if (_SpawnRow && xx!=0 && xx!=_numTilesCols-1)
					{
						var validPos:Boolean = false;
						for (var jj:uint = yy; jj < _numTilesRows ; ++jj)
						{
								if (mat[jj][xx] == 1 && mat[jj][xx-1] == 1 && mat[jj][xx+1] == 1)
								{
									validPos = true;
								}
						}
						if (validPos)
						{
							var AorBValue:uint = 0;
							if((!_playerAPlaced && !_playerBPlaced) || (_playerAPlaced && _playerBPlaced))
								AorBValue = (Math.random() < 0.5 ? 4:5);
							else if (_playerAPlaced)
								AorBValue = 5;
							else if (_playerBPlaced)
								AorBValue = 4;
							if (Math.random() < _initPlayerRatio)
							{
								mat[yy][xx] = AorBValue;
								if (AorBValue == 4) {
									_playerAPlaced = true;
								}else{
									_playerBPlaced = true;
								}
							}
						}
						else
						{
							_initPlayerRatio += 0.05;
						}
					}
					
					/**
					 * Set Collectables Spawns
					 */
					if (_CollectablesRow && xx != 0 && xx != _numTilesCols-1)
					{
						var GorWValue:uint = 0;
						if((!_GPlaced && !_FCPlaced))
							GorWValue = (Math.random() < 0.5 ?6:7);
						else if (_GPlaced && _FCPlaced)
						{
							GorWValue = (Math.random() < 0.5 ?6:7);
							_GPlaced = false;
							_FCPlaced = false;
						}
						else if (_GPlaced)
							GorWValue =7;
						else if (_FCPlaced)
							GorWValue = 6;
						if (Math.random() < _initCollectableRatio)
							{
								
								mat[yy][xx] = GorWValue;
								if (GorWValue == 6)
									_GPlaced = true;
								else
									_FCPlaced = true;
							}
					}
				}
				
			}
			return mat;
		}
		
	}

}