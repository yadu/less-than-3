package india.gamedev.stag
{
	import india.gamedev.stag.utils.FlxForestGenerator;
	
	/**
	 * ...
	 * @author Yadu Rajiv
	 */
	public class Resources 
	{
		
		//[Embed(source = "data/newtile.png")] public static var tileset:Class;
		[Embed(source = "data/tileset.png")] public static var tileset:Class;
		[Embed(source = "data/forest_tileset.png")] public static var forest_tileset:Class;
		//[Embed(source = "data/level2_brickTiles.png")] public static var tileset:Class;
		
		[Embed(source = "data/ProfilePic.jpg")] public static var logo:Class;
		
		[Embed(source = "data/levels/testlevel.png")] public static var testmap:Class;

		[Embed(source = "data/Dad.png")] public static var img_dad:Class;
		[Embed(source = "data/Daughter.png")] public static var img_daughter:Class;
		
		[Embed(source = "data/boy1.png")] public static var img_boy1:Class;
		[Embed(source = "data/boy2.png")] public static var img_boy2:Class;
		
		[Embed(source = "data/bg.png") ]public static var img_bg:Class;
		[Embed(source = "data/Splash.png") ]public static var img_menubg:Class;
		[Embed(source = "data/bga.png") ]public static var img_bga:Class;
		
		[Embed(source = "data/Player1.png")]public static var img_player1win:Class;
		[Embed(source = "data/Player2.png")]public static var img_player2win:Class;
		[Embed(source = "data/draw.png")]public static var img_draw:Class;
		[Embed(source = "data/instructions.png")]public static var img_instructions:Class;
		
		[Embed(source = "data/coinx.png")] public static var img_coin:Class;
		[Embed(source = "data/powerx.png")] public static var img_powerup:Class;
		
		[Embed(source = "data/audio/collect_0.mp3")] static public var snd_collect:Class;
		[Embed(source = "data/audio/jump_0.mp3")] static public var snd_jump0:Class;
		[Embed(source = "data/audio/jump_1.mp3")] static public var snd_jump:Class;
		[Embed(source = "data/audio/lowHit0.mp3")] static public var snd_hit:Class;
		[Embed(source = "data/audio/As_I_Rover_Final.mp3")] static public var mus_rover:Class;
		[Embed(source = "data/audio/TheDarkReprise.mp3")] static public var mus_menu:Class;
		
		[Embed(source = "data/audio/taunt_1.mp3")] static public var snd_taunt1:Class;
		[Embed(source = "data/audio/taunt_2.mp3")] static public var snd_taunt2:Class;
		[Embed(source = "data/audio/taunt_3.mp3")] static public var snd_taunt3:Class;
		[Embed(source = "data/audio/win.mp3")] static public var snd_win:Class;
		[Embed(source = "data/audio/omg.mp3")] static public var snd_OMG:Class;
		
		[Embed(source = "data/audio/are_you.mp3")] static public var snd_hate1:Class;
		[Embed(source = "data/audio/cheap_bastard.mp3")] static public var snd_hate2:Class;
		[Embed(source = "data/audio/do_you_love.mp3")] static public var snd_hate3:Class;
		
		public static var hate:Array = [snd_hate1, snd_hate2, snd_hate3];
		
		[Embed(source = "data/audio/negative_power_up.mp3")] static public var snd_negative:Class;
		[Embed(source = "data/audio/inverted_controls.mp3")] static public var snd_inverted:Class;
		[Embed(source = "data/audio/control_transferred.mp3")] static public var snd_control:Class;
		[Embed(source = "data/audio/freezed.mp3")] static public var snd_freezed:Class;
		
		private var test:FlxForestGenerator;
		
		public static var winner:String;
		
	}
	
}