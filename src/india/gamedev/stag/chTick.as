package india.gamedev.stag 
{
	
	/**
	 * ...
	 * @author Yadu Rajiv
	 */
	public class chTick
	{
		private var _tms:Number;
		private var _tsecs:Number;
		private var _tmins:Number;
		private var _thrs:Number;
		private var _tdays:Number;
		private var _totalElapsed:Number;
		
		public function chTick() {
			_totalElapsed = _tms = _tsecs = _tmins = _thrs = _tdays = 0;
		}
		
		public function tick(timeElapsed:Number):Number {
			_tms += timeElapsed;
			
			_tsecs += timeElapsed;
			_totalElapsed += timeElapsed;
			
			if (_tms >= 1) {
				_tms = 0;
			}
			
			if(_totalElapsed > 0) {
				updateTick();
			} else {
				_totalElapsed = 0;
				_tsecs = 0;
				updateTick();
			}
			return _totalElapsed;
		}
		
		private function updateTick():void {
			
			if (_thrs >= 24) {
				_tdays++;
				_thrs = 0;
			}
			
			if (_tmins >= 60) {
				_thrs++;
				_tmins = 0;
			}
			
			if (_tsecs >= 60) {
				_tmins++;
				_tsecs = _tsecs - 60;
			}
			
		}
		
		public function s():Number {
			return _tsecs;
		}
		
		public function m():Number {
			return _tmins;
		}
		
		public function h():Number {
			return _thrs;
		}
		
		public function ms():Number {
			return _tms;
		}
		
		public function totalElapsed():Number {
			return _totalElapsed;
		}
	}
	
}