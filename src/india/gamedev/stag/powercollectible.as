package india.gamedev.stag 
{
	import flash.automation.StageCapture;
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author Sanchit Gulati
	 */
	public class powercollectible extends FlxSprite
	{
		
		// can be normal, special, key
		public var itemType:String;
		
		public function powercollectible(X:Number = 0, Y:Number = 0, Type:String = "", SimpleGraphic:Class = null) {
			super(X, Y, SimpleGraphic);
			
			if (SimpleGraphic == null) {
				makeGraphic(16, 16, 0xff00ff00);//32x32
			}
			
			itemType = Type;
		}
		
		override public function destroy():void 
		{
			itemType = null
			
			super.destroy();
		}
	}
	
}