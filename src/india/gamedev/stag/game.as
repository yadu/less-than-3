package india.gamedev.stag
{
	import flash.display.ColorCorrection;
	import flash.geom.Point;
	import india.gamedev.stag.utils.ArrayClone;
	import india.gamedev.stag.utils.FlxForestGenerator;
	import org.flixel.*;
	
	/**
	 * ...
	 * @author Yadu Rajiv
	 */
	public class game extends FlxState 
	{
		
		public var baseMap:FlxTilemap;
		public var p1:player;
		public var p2:player;
		private var xmap:uint = 80;
		private var ymap:uint = 48;
		private var _forestMatrix:Array;
		
		private var textDimensions:FlxText;
		
		private var dadSprite:FlxSprite;
		private var daughterSprite:Daughter;
		
		private var playerASpawns:Array;
		private var playerBSpawns:Array;
		private var p1spawn:FlxPoint = null;
		private var p2spawn:FlxPoint = null;
		private var giftSpawns:Array;
		private var powerSpawns:Array;
		
		private var groupGift:FlxGroup;
		private var groupPower:FlxGroup;
		
		private var giftSpawnTime:Number;
		
		private var hudGroup:FlxGroup;
		
		private var playerOneScore:FlxText;
		private var playerTwoScore:FlxText;
		
		private var playerOneCoinStatus:FlxText;
		private var playerTwoCoinStatus:FlxText;
		
		
		private var notificationPlayerOne:FlxText;
		private var notificationPlayerTwo:FlxText;
		
		private var tick:chTick;
		private var lastTimeTaunt:uint;
		private var hudTimer:FlxText;
		
		override public function create():void 
		{
			super.create();
			
			// setup level
			setup();
			
		}
		
		public function setup():void {
			
			//bg 
			add(new FlxSprite(0, 0, Resources.img_bga));
			
			
			// map
			// add tilemap
			baseMap = new FlxTilemap();
			
			var forest:FlxForestGenerator = new FlxForestGenerator(xmap,ymap);
			// Generate the level and returns a matrix
			_forestMatrix = forest.generateForestLevel();
				
			// Converts the matrix into a string that is readable by FlxTileMap even special numerics
			//var dataStr:String = FlxForestGenerator.convertMatrixToStr( _forestMatrix );

			// Converts the matrix into a string that is readable by FlxTileMap only 1:0's
			var dataStr:String = FlxForestGenerator.convertMatrixToStrBinaryOnly( ArrayClone.clone(_forestMatrix) );
			baseMap.loadMap(dataStr, Resources.tileset, 10, 10, FlxTilemap.AUTO);
			add(baseMap); // add map to scene
			
			// mouse show
			FlxG.mouse.show();
			
			playerASpawns = new Array();
			playerBSpawns = new Array();
			giftSpawns = new Array();
			powerSpawns = new Array();
			
			/**
			 * Traverse the map and create objects based on numeric values
			 */
			for (var y:uint = 0; y < ymap; ++y)
			{
				for (var x:uint = 0; x < xmap; ++x)
				{
					/*
					 * // no more fire! hole are the in thing!
					if (_forestMatrix[y][x] == 8)
					{
						add(new fire(x * 10, y * 10, 0xffff0033));
					}
					*/
					
					/**
					 * player spawn points
					 */
					if (_forestMatrix[y][x] == 4) // player 1
					{
						playerASpawns.push(new FlxPoint(x * 10, y * 10));
					}					
					if (_forestMatrix[y][x] == 5) // player 2
					{
						playerBSpawns.push(new FlxPoint(x * 10, y * 10));
					}
					
					/**
					 * gifts
					 */
					if (_forestMatrix[y][x] == 6)
					{
						//store spawn points
						giftSpawns.push(new FlxPoint(x * 10, y * 10));
					}
					
					/**
					 * power-ups
					 */
					if (_forestMatrix[y][x] == 7)
					{
						//store power-ups
						powerSpawns.push(new FlxPoint(x*10,y*10));
					}
				}
			}
			
			// debug full screen dimensions
			//textDimensions = new FlxText(10, 10, 200, "")
			//add(textDimensions);
			
			// player spawnning 
			spawnPlayerOne();
			spawnPlayerTwo();
			
			// daughter sprite
			daughterSprite = new Daughter(FlxG.width/2 - 16, 40, Resources.img_daughter);
			add(daughterSprite);
			
			// collectibles
			spawnGift();
			spawnPower();
			
			
			// setting up hud
			hudGroup = new FlxGroup();
			playerOneScore = new FlxText(10, 10, 200, "00");
			playerOneScore.color = 0xffff0000;
			playerOneScore.size = 14;
			hudGroup.add(playerOneScore);
			
			playerTwoScore = new FlxText(FlxG.width - 210, 10, 200, "00");
			playerTwoScore.alignment = "right";
			playerTwoScore.color = 0xffff0000;
			playerTwoScore.size = 14;
			hudGroup.add(playerTwoScore);
			
			playerOneCoinStatus = new FlxText(10, 28, 200, "");
			playerOneCoinStatus.alignment = "left";
			playerOneCoinStatus.color = 0xffff0000;
			playerOneCoinStatus.size = 14;
			hudGroup.add(playerOneCoinStatus);
			
			playerTwoCoinStatus = new FlxText(FlxG.width - 210, 28, 200, "");
			playerTwoCoinStatus.alignment = "right";
			playerTwoCoinStatus.color = 0xffff0000;
			playerTwoCoinStatus.size = 14;
			hudGroup.add(playerTwoCoinStatus);
			
			notificationPlayerOne = new FlxText(FlxG.width/2 - 210, 60, 200, "");
			notificationPlayerOne.alignment = "center";
			notificationPlayerOne.color = 0xffff0000;
			notificationPlayerOne.size = 14;
			hudGroup.add(notificationPlayerOne);
			
			
			notificationPlayerTwo = new FlxText(FlxG.width/2 - 210, 70, 200, "");
			notificationPlayerTwo.alignment = "center";
			notificationPlayerTwo.color = 0xffff0000;
			notificationPlayerTwo.size = 14;
			hudGroup.add(notificationPlayerTwo);
			
			hudTimer = new FlxText(FlxG.width / 2 - 100, 10, 200, "00:00:000");
			hudTimer.alignment = "center";
			hudTimer.color = 0xffff0000;
			hudTimer.size = 14;
			hudGroup.add(hudTimer);
			
			tick = new chTick();
			lastTimeTaunt = 0;

			//music
			FlxG.music.stop(); // stop menu music
			FlxG.playMusic(Resources.mus_rover, (FlxG.volume>=4)?FlxG.volume-4:2);
			
			add(hudGroup);
		}
		
		/**
		 * spawns a group with 4 gifts or revives them
		 */
		public function spawnGift():void {
			groupGift = new FlxGroup();
			for (var i:uint = 0; i < 4; i++) {
				addGiftToGroup();
			}
			add(groupGift);
		}
		/**
		 * spawns a group with 4 power 
		 */
		public function spawnPower():void {
			groupPower = new FlxGroup();
			for (var j:uint = 0; j < 4; j++ )
			{
				addPowerToGroup();
			}
			add(groupPower);
			}
		
		public function spawnCoin():void {
			// if any coins are taken, respawn a coin XD
			if (groupGift.countDead() > 0) {
				addGiftToGroup();
			}
		}
		
		public function spawnPlayerOne():void {
			
			if(p1spawn == null && p2spawn != null) {
				while(FlxU.getDistance(p1spawn, p2spawn) < 75) {
					p1spawn = FlxU.getRandom(playerASpawns) as FlxPoint;
				}
			} else {
				p1spawn = FlxU.getRandom(playerASpawns) as FlxPoint;
			}
			
			p1 = new player(p1spawn.x,p1spawn.y, 0xffffffff, Resources.img_boy1); // img_player1
			p1.useAlt = true;
			p1.width = 14;
			p1.height = 24;
			p1.offset = new FlxPoint(9, 4);
			add(p1);
		}
		
		public function spawnPlayerTwo():void {
			
			if (p1spawn != null && p2spawn == null) {
				p2spawn = FlxU.getRandom(playerBSpawns) as FlxPoint;
				while(FlxU.getDistance(p1spawn, p2spawn) < 75) {
					p2spawn = FlxU.getRandom(playerBSpawns) as FlxPoint;
				}
			} else {
				p2spawn = FlxU.getRandom(playerBSpawns) as FlxPoint;
			}
			
			p2 = new player(p2spawn.x,p2spawn.y, 0xffffffff, Resources.img_boy2); // img_player2
			p2.useAlt = false;
			p2.width = 14;
			p2.height = 24;
			p2.offset = new FlxPoint(9, 4);
			add(p2);
		}
		/**
		 * adding a power to the group of power if they are not already there
		 * @return new or old collectible
		 */
		public function addPowerToGroup() {
			var pcol:powercollectible = null;
			var pcolSpawn:FlxPoint = FlxU.getRandom(powerSpawns) as FlxPoint;
				

				if (Math.random() <= 0.25)
				{
					pcol = new powercollectible(pcolSpawn.x, pcolSpawn.y, "inverse");
				}
				else if(Math.random() <= 0.50)
				{
					pcol = new powercollectible(pcolSpawn.x, pcolSpawn.y, "freeze");
				}
				else if(Math.random() <= 0.75)
				{
					pcol = new powercollectible(pcolSpawn.x, pcolSpawn.y, "negative");
				}
				else if(Math.random() <= 1.0)
				{
					pcol = new powercollectible(pcolSpawn.x, pcolSpawn.y, "control");
				}
				
				pcol.loadGraphic( Resources.img_powerup, true);
				pcol.addAnimation("idle", [0, 1, 2], 6, true);
				pcol.play("idle");
				groupPower.add(pcol)
		
		}
		/**
		 * adding a gift to the group of gifts if they are not already there, or revive an existing one
		 * 
		 * @return new or old collectible
		 */
		public function addGiftToGroup():FlxBasic {
			
			var col:collectible = null;
			var colSpawn:FlxPoint = FlxU.getRandom(giftSpawns) as FlxPoint;
			
			if(groupGift.length < 4) {
				
				if((FlxU.getDistance(colSpawn,p1spawn) < 200)  || (FlxU.getDistance(colSpawn,p2spawn) < 200)) {
					col = new collectible(colSpawn.x, colSpawn.y, "gift", 5);
				} else {
					col = new collectible(colSpawn.x, colSpawn.y, "gift", 10);
					col.color = 0xffff0000;
				}
				
				col.loadGraphic( Resources.img_coin, true);
				col.addAnimation("idle", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], 12, true);
				col.play("idle");
				
				return groupGift.add(col);
				
				
			} else if(groupGift.countDead() > 0) {
				col = groupGift.getFirstDead() as collectible;
				col.x = colSpawn.x;
				col.y = colSpawn.y;
				col.revive();
			}
			
			return col;
		}
		
		override public function update():void 
		{
			
			// seems legit XD
			tick.tick(FlxG.elapsed);
			FlxG.collide(p1, p2, function(obj:player, obj2:player):void {
				if (tick.s()-lastTimeTaunt >= 5)
				{
					lastTimeTaunt = tick.s();
					if (Math.random() <= 0.33)
						FlxG.play(Resources.snd_taunt1, FlxG.volume);
					else if (Math.random() <= 0.66)
						FlxG.play(Resources.snd_taunt2, FlxG.volume);
					else
						FlxG.play(Resources.snd_taunt3, FlxG.volume);
				}
			});
				
			FlxG.collide(p1, baseMap);
			FlxG.collide(p2, baseMap);
			
			if (FlxG.keys.justPressed("P"))
			{
				FlxG.switchState(new game());
			}
			
			/**
			 * player gift collision
			 */
			FlxG.overlap(p1, groupGift, function(obj:player, obj2:collectible):void {
				if (!p1.hasGift) {
					p1.itemType = obj2.itemType;
					p1.itemValue = obj2.itemValue;
					p1.hasGift = true;
					obj2.kill();
					FlxG.play(Resources.snd_collect, FlxG.volume);
				}
			});
			
			FlxG.overlap(p2, groupGift, function(obj:player, obj2:collectible):void {
				if (!p2.hasGift) {
					p2.itemType = obj2.itemType;
					p2.itemValue = obj2.itemValue;
					p2.hasGift = true;
					obj2.kill();
				}
			});
			
			/**
			 * Disables power time based
			 */

			if (p1.powerActivated = true && (tick.s() - p1.powerTick >= 4)) //hardcoded
			{
				p1.powerTick = 0;
				p1.powerActivated = false;
				if (p1.powerType == "freeze")
					p2.deadStick = false;
				if (p1.powerType == "inverse")
					p2.inverseControl = false;
				if (p1.powerType == "control")
					{
						p1.deadStick = false;
						p2.useAlt = !p2.useAlt;
					}
				p1.powerType = "";
				notificationPlayerOne.text = "";
			}
			if (p2.powerActivated = true && (tick.s() - p2.powerTick  >= 4))   //hardcoded
			{
				p2.powerTick = 0;
				p2.powerActivated = false;
				if (p2.powerType == "freeze")
					p1.deadStick = false;
				if (p2.powerType == "inverse")
					p1.inverseControl = false;
				if (p2.powerType == "control")
					{
						p2.deadStick = false;
						p2.useAlt = !p2.useAlt;
					}
				p2.powerType = "";
				notificationPlayerTwo.text = "";
			}
			
			/**
			 * Player overlap with power
			 */
			if(!p1.powerActivated)
			FlxG.overlap(p1, groupPower, function(obj:player, obj2:powercollectible):void {
					p1.powerType = obj2.itemType;
					p1.powerTick = tick.s();
					p1.powerActivated = true;
					//implements power here
					if (p1.powerType == "negative")
					{
						daughterSprite.playerTwoLove -= 6;
						FlxG.play(Resources.snd_negative, FlxG.volume);
					}
					else if (p1.powerType == "freeze")
					{
						p2.deadStick = true;
						FlxG.play(Resources.snd_freezed, FlxG.volume);
					}
					else if (p1.powerType == "control")
					{
						p2.useAlt = !p2.useAlt;
						p1.deadStick = true;
						FlxG.play(Resources.snd_control, FlxG.volume);
					}
					else if (p1.powerType == "inverse")
					{
						p2.inverseControl = true;
						FlxG.play(Resources.snd_inverted, FlxG.volume);
					}
					obj2.kill();
			
			});
			
			if(!p2.powerActivated)
			FlxG.overlap(p2, groupPower, function(obj:player, obj2:powercollectible):void {
					p2.powerType = obj2.itemType;
					p2.powerTick = tick.s();
					p2.powerActivated = true;
					//implements power here
					if (p2.powerType == "negative")
						{
							daughterSprite.playerOneLove -= 6;
							FlxG.play(Resources.snd_negative, FlxG.volume);
						}
					else if (p2.powerType == "freeze")
					{
						p1.deadStick = true;
						FlxG.play(Resources.snd_freezed, FlxG.volume);
					}
					else if (p2.powerType == "control")
					{
						p1.useAlt = !p1.useAlt;
						p2.deadStick = true;
						FlxG.play(Resources.snd_control, FlxG.volume);
					}
					else if (p2.powerType == "inverse")
						{
							p1.inverseControl = true;
							FlxG.play(Resources.snd_inverted, FlxG.volume);
						}
					obj2.kill();
			});
			
			//player 2 here with gift meh
			
			// player uno sexytime!
			FlxG.overlap(p1, daughterSprite, function(obj:player, obj2:Daughter):void {
				if (p1.hasGift) {
					FlxG.play(FlxG.getRandom(Resources.hate) as Class, FlxG.volume);
					daughterSprite.playerOneLove += p1.itemValue;
					p1.hasGift = false;
					p1.itemType = "";
					p1.itemValue = 0;
					spawnCoin();
				}
			});
			
			// player duo sexytime!
			FlxG.overlap(p2, daughterSprite, function(obj:player, obj2:Daughter):void {
				if (p2.hasGift) {
					FlxG.play(FlxG.getRandom(Resources.hate) as Class, FlxG.volume);
					daughterSprite.playerTwoLove += p2.itemValue;
					p2.hasGift = false;
					p2.itemType = "";
					p2.itemValue = 0;
					spawnCoin();
				}
			});
			
			
			
			// player one dies
			if (p1.x > FlxG.width || p1.x < 0 || p1.y > FlxG.height || p1.y < 0 || p1.health <= 0) {
				if(p1.hasGift) {
					spawnCoin(); //when player dies with a coin in his hand
				}
				p1.kill();
				remove(p1);
				p2.deadStick = false;
				p2.powerActivated = false;
				p1.useAlt = true;
				p2.useAlt = false;
				spawnPlayerOne();
			}
			
			// player two dies
			if (p2.x > FlxG.width || p2.x < 0 || p2.y > FlxG.height || p2.y < 0 || p2.health <= 0) {
				if(p2.hasGift) {
					spawnCoin(); //when player dies with a coin in his hand
				}
				p2.kill();
				remove(p2);
				p1.deadStick = false;
				p1.useAlt = true;
				p2.useAlt = false;
				p1.powerActivated = false;
				spawnPlayerTwo();
			}
			
			// updating player score to hud item
			playerOneScore.text = "1UP - " + daughterSprite.playerOneLove.toString();
			playerTwoScore.text = "2UP - " + daughterSprite.playerTwoLove.toString();
			
			//has coins ???
			playerOneCoinStatus.text =  p1.hasGift?"Has Gift":"Let some gift";
			playerTwoCoinStatus.text =  p2.hasGift?"Has Gift":"Let some gift";
			
			
			// updating timer
			hudTimer.text = "00:" + flxExtra.fix(FlxU.floor(60 - tick.s())) + ":" + flxExtra.fixms(tick.ms());
			
			if (tick.s() == 48) {
				hudTimer.flicker(12);
			}
			
			// 60 second rule
			if (tick.totalElapsed() >= 60) { // hardcoded 60
				if (daughterSprite.playerOneLove > daughterSprite.playerTwoLove) {
					Resources.winner = "Player One Wins!";
				} else if (daughterSprite.playerTwoLove > daughterSprite.playerOneLove) {
					Resources.winner = "Player Two Wins!";
				} else {
					Resources.winner = "No one Wins!";
				}
				
				FlxG.switchState(new win);
			}
			
			super.update();
			
		}
		
		override public function destroy():void 
		{
			// NULLs here
			
			super.destroy();
		}
		

		
	}
	
}